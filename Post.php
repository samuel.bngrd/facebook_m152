<?php
require_once 'database_php/requetes.php';

$description = filter_input(INPUT_POST, "description", FILTER_DEFAULT);
$action = filter_input(INPUT_POST, 'action');

if ($action == "Post") {
	
	for ($i=0; $i < count($_FILES["image"]["name"]); $i++) { 
		$myName = $_FILES["image"]["name"][$i];
		$fileextension = explode(".", $myName);
		$myextension = strtolower($fileextension[1]);

		Post($myName, $myextension, $description);
		//Save the file
		$uploads_dir = './uploads';
	}

	
	foreach ($_FILES["image"]["error"] as $key => $error) {
		if ($error == UPLOAD_ERR_OK) {
			$tmp_name = $_FILES["image"]["tmp_name"][$key];
			// basename() may prevent filesystem traversal attacks;
			// further validation/sanitation of the filename may be appropriate
			$name = basename($_FILES["image"]["name"][$key]);
			move_uploaded_file($tmp_name, "$uploads_dir/$name");
		}

	}
	header('Location: Home.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Facebook Publication</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="assets/css/bootstrap.css" rel="stylesheet">
	<!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<link href="assets/css/facebook.css" rel="stylesheet">
</head>

<body>
	<!-- top nav -->
	<div class="navbar navbar-blue navbar-static-top">
		<div class="navbar-header">
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="http://usebootstrap.com/theme/facebook" class="navbar-brand logo">b</a>
		</div>
		<nav class="collapse navbar-collapse" role="navigation">
			<form class="navbar-form navbar-left">
				<div class="input-group input-group-sm" style="max-width:360px;">
					<input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
					<div class="input-group-btn">
						<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
					</div>
				</div>
			</form>
			<ul class="nav navbar-nav">
				<li>
					<a href="Home.php"><i class="glyphicon glyphicon-home"></i> Home</a>
				</li>
				<li>
					<a href="Post.php" role="button" data-toggle="modal"><i class="glyphicon glyphicon-plus"></i> Post</a>
				</li>
				<li>
					<a href="#"><span class="badge">badge</span></a>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i></a>
					<ul class="dropdown-menu">
						<li><a href="">More</a></li>
						<li><a href="">More</a></li>
						<li><a href="">More</a></li>
						<li><a href="">More</a></li>
						<li><a href="">More</a></li>
					</ul>
				</li>
			</ul>
		</nav>
	</div>

	<!--post modal-->
	<form action="Post.php" method="post" id="postModal" tabindex="-1" enctype="multipart/form-data">
		<br>
		<br>
		<br>
		<div class="modal-content">
			<div class="modal-body">
				<div class="form-group">
					<textarea class="form-control input-lg" autofocus="" placeholder="What do you want to share?" name="description"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<div>
					<input type="submit" class="btn btn-primary btn-sm" aria-hidden="true" name="action" value="Post">
					<ul class="pull-left list-inline">
						<input type="file" name="image[]" multiple accept="image/x-png,image/gif,image/jpeg,video/mp4">
					</ul>
				</div>
			</div>
		</div>
	</form>


	<script type="text/javascript" src="assets/js/jquery.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('[data-toggle=offcanvas]').click(function() {
				$(this).toggleClass('visible-xs text-center');
				$(this).find('i').toggleClass('glyphicon-chevron-right glyphicon-chevron-left');
				$('.row-offcanvas').toggleClass('active');
				$('#lg-menu').toggleClass('hidden-xs').toggleClass('visible-xs');
				$('#xs-menu').toggleClass('visible-xs').toggleClass('hidden-xs');
				$('#btnShow').toggle();
			});
		});
	</script>
</body>

</html>