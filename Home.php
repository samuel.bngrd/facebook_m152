<?php
require_once 'database_php/requetes.php';

//On va chercher tout les médias et on ajoute l'idPost afin de pouvoir identifier dans quel post va chaque image
function chargerMedia()
{
	$media = lireMedia();
	$mesMedia = [];

	for ($i = 0; $i < count($media); $i++) {
		$typeMedia = $media[$i]["typeMedia"];
		$monMedia = [];
		//Si c'est une vidéo
		if ($typeMedia == "mp4") {
			$monMedia[0] = '<video width="200" height="200" controls="controls">
				<source src="uploads/' . $media[$i]["nomFichierMedia"] . '" type="video/' . $typeMedia . '" height="400" width="400">/>
				Your browser does not support the video element.
				</video>';
		}
		//Si c'est une fichier audio
		else if ($typeMedia == "riff" || $typeMedia == "wav" || $typeMedia == "bwf" || $typeMedia == "ogg" || $typeMedia == "aiff" || $typeMedia == "caf" || $typeMedia == "raw") {
			$monMedia[0] = '<audio controls>
				<source src="' . $media[$i]["nomFichierMedia"] . '"" type="audio/' . $typeMedia . '>" height="400" width="400">
				  Your browser does not support the audio element.
				  </audio>';
		}
		//Sinon c'est une image
		else {
			$monMedia[0] = '<img src="uploads/' . $media[$i]["nomFichierMedia"] . '" class="img-responsive" height="400" width="400">';
		}
		$monMedia[1] = $media[$i]["idPost"];
		$mesMedia[$i] = $monMedia;
	}
	return $mesMedia;
}

function AfficherPost()
{
	$post = lirePost();
	$mesPost = "";
	$tableauMedia = chargerMedia();
	$mesMedia = "";

	for ($i = 0; $i < count($post); $i++) {
		$idPost = $post[$i]["idPost"];

		for ($i=0; $i < count($tableauMedia); $i++) { 
			if($tableauMedia[$i][1] = $idPost){
				$mesMedia .= $tableauMedia[$i][0];
			}
		}

		$mesPost = sprintf('<div class="panel panel-body">
		<div class="panel-thumbnail" class="img-responsive"></div> 
		<div class="panel-body">
			%s
			<div class="text-right">
				<img src="icons/edit.png" class="" height="42" width="42">
				<a href="PHP/deletePost.php?idPost=%s"><img src="icons/del.png" class="" height="42" width="42"></a>
			</div>
			%s
		</div>
	</div>', $mesMedia, $post[$i]["idPost"], $post[$i]["commentaire"]);
	}
	return $mesPost;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Facebook Theme Demo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="assets/css/bootstrap.css" rel="stylesheet">
	<!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
	<link href="assets/css/facebook.css" rel="stylesheet">
</head>

<body>
	<div class="wrapper">
		<div class="box">
			<div class="row row-offcanvas row-offcanvas-left">

				<!-- sidebar -->

				<!-- /sidebar -->

				<!-- main right col -->
				<div class="column col-sm-10 col-xs-11" id="main">

					<!-- top nav -->
					<div class="navbar navbar-blue navbar-static-top">
						<div class="navbar-header">
							<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a href="http://usebootstrap.com/theme/facebook" class="navbar-brand logo">b</a>
						</div>
						<nav class="collapse navbar-collapse" role="navigation">
							<form class="navbar-form navbar-left">
								<div class="input-group input-group-sm" style="max-width:360px;">
									<input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
									<div class="input-group-btn">
										<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
									</div>
								</div>
							</form>
							<ul class="nav navbar-nav">
								<li>
									<a href="#"><i class="glyphicon glyphicon-home"></i> Home</a>
								</li>
								<li>
									<a href="Post.php" role="button" data-toggle="modal"><i class="glyphicon glyphicon-plus"></i> Post</a>
								</li>
								<li>
									<a href="#"><span class="badge">badge</span></a>
								</li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i></a>
									<ul class="dropdown-menu">
										<li><a href="">More</a></li>
										<li><a href="">More</a></li>
										<li><a href="">More</a></li>
										<li><a href="">More</a></li>
										<li><a href="">More</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
					<!-- /top nav -->

					<div class="padding">
						<div class="full col-sm-9">

							<!-- content -->
							<div class="row">

								<!-- main col left -->
								<div class="col-sm-5">

									<div class="panel panel-default">
										<div class="panel-thumbnail"><img src="assets/img/bg_5.jpg" class="img-responsive"></div>
										<div class="panel-body">
											<p class="lead">Urbanization</p>
											<p>45 Followers, 13 Posts</p>

											<p>
												<img src="assets/img/uFp_tsTJboUY7kue5XAsGAs28.png" height="28px" width="28px">
											</p>
										</div>
									</div>


								</div>

								<!-- main col right -->
								<div class="col-sm-7">
									<div class="panel panel-default">
										<div class="panel-body">
											<h2>WELCOME</h2>
										</div>
									</div>
									<?= AfficherPost() ?>
								</div>
							</div>
							<!--/row-->
							<div class="row" id="footer">
							</div>

							<hr>

							<h4 class="text-center">
								<a href="http://usebootstrap.com/theme/facebook" target="ext">Download my Template @Bootply</a>
							</h4>

							<hr>


						</div><!-- /col-9 -->
					</div><!-- /padding -->
				</div>
				<!-- /main -->

			</div>
		</div>
	</div>



</body>

</html>