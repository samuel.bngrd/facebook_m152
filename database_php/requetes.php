<?php
require_once 'database.php';
echo "<br>";
echo "<br>";
echo "<br>";
function lastId()
{
    $sql = "SELECT max(idPost) as idPost FROM `bdfacebook`.`post`;";
    $query = connect()->prepare($sql);
    $query->execute();
    $id = $query->fetchall(PDO::FETCH_ASSOC);
    return $id;
}

function Post($nomFichierMedia, $typeMedia, $commentaire)
{
    try {
        $sql = "INSERT INTO bdfacebook.post(commentaire) VALUES (:commentaire);";
        $query = connect()->prepare($sql);
        $query->execute([
            ':commentaire' => $commentaire,
        ]);

        $id = lastId();

        $sql = "INSERT INTO bdfacebook.media(nomFichierMedia, typeMedia, idPost) VALUES (:nomFichierMedia, :typeMedia, :id);";
        $query = connect()->prepare($sql);
        $query->execute([
            ':nomFichierMedia' => $nomFichierMedia,
            ':typeMedia' => $typeMedia,
            ':id' => (string) $id[0]["idPost"],
        ]);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

function lireMedia()
{
    $sql = "SELECT * FROM `bdfacebook`.`media` order by creationDate desc;";

    $query = connect()->prepare($sql);
    $query->execute();
    $media = $query->fetchall(PDO::FETCH_ASSOC);
    return $media;
}

function lirePost()
{
    $sql = "SELECT * FROM `bdfacebook`.`post`;";

    $query = connect()->prepare($sql);
    $query->execute();
    $media = $query->fetchall(PDO::FETCH_ASSOC);
    return $media;
}

function deletePost($idPost)
{
    deleteMedia($idPost);  

    //Supprimer ligne dans la table post
    $sql = "DELETE FROM `post` WHERE `idPost` = :idPost;";

    $query = connect()->prepare($sql);

    return $query->execute([
        ':idPost' => $idPost
    ]);
}

function deleteMedia($idPost)
{
    //Supprimer ligne dans la table post
    $sql = "DELETE FROM `media` WHERE `idPost` = :idPost;";

    $query = connect()->prepare($sql);

    return $query->execute([
        ':idPost' => $idPost
    ]);
}

