<?php

function connect() {
    static $myDb = NULL;
    $dbName = "bdFacebook";
    $dbUser = "admFacebook";
    $dbPass = "Super2020";
    if ($myDb === NULL) {
        try {
            $myDb = new PDO(
                "mysql:host=localhost;dbname=$dbName;charset=utf8",
                $dbUser,
                $dbPass,
                array(
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_EMULATE_PREPARES => false
                )
            );
        } catch (Exception $ex) {
            die("Impossbile de se connecter à la base " . $ex->getMessage());
        }
    }
    return $myDb;
}